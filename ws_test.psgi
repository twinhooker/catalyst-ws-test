use strict;
use warnings;

use lib $ENV{'HOME'} . '/devel/ws_test/lib';
use ws_test;

my $app = ws_test->apply_default_middlewares(ws_test->psgi_app);
$app;


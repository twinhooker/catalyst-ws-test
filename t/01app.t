#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;

use Catalyst::Test 'ws_test';

ok( request('/')->is_success, 'Request should succeed' );

done_testing();

use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ws_test';
use ws_test::Controller::WS;

ok( request('/ws')->is_success, 'Request should succeed' );
done_testing();

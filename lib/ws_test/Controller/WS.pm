package ws_test::Controller::WS;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

ws_test::Controller::WS - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ($self, $c) = @_;
    my $url = 'localhost:5000/ws/ws';

    $c->stash(template => 'ws/index.tt');
    #    $c->response->body('Matched ws_test::Controller::WS in WS.');
}

=head2 ws

=cut

use Protocol::WebSocket::Handshake::Server;
use AnyEvent::Handle;
use AnyEvent;
use AnyEvent::Pg;
use Data::Dumper;
my %conns;

sub ws_test :Path(/ws/ws_test) {
    my ($self, $ctx) = @_;

    my $hs = Protocol::WebSocket::Handshake::Server->new_from_psgi($ctx->req->env);
    my $hd = AnyEvent::Handle->new(
        fh => $ctx->req->io_fh,
        on_error => sub { warn "Error ".pop }
    );

    $hs->parse($hd->fh);
    
    $hd->push_write($hs->to_string);
    $hd->push_write($hs->build_frame(buffer => "Echo Initiated")->to_bytes);
 
    $hd->on_read(
        sub {
            #warn Dumper(@_);
            
            (my $frame = $hs->build_frame)->append($_[0]->rbuf);
            while (my $message = $frame->next) {
                warn $message;
                $message = $hs->build_frame(buffer => $message)->to_bytes;                
                $hd->push_write($message);
            }        
        }
    );

    my $dbh   = $ctx->model('WSModel')->dbh;    

    $dbh->do("LISTEN foo");
    my $db_fd = $dbh->func('getfd');
    my $db_io = AE::io $db_fd, 0, sub {
        my $notify = $dbh->func("pg_notifies");
        $conns{$hd}{hd}->push_write($hs->build_frame(buffer => "qwerqerqwe")->to_bytes);        
    };
    
    my $timer = AE::timer 0, 5, sub {
        #warn Dumper(\%conns);
        #warn $hd;
        
        $conns{$hd}{hd}->push_write($hs->build_frame(buffer => "ping")->to_bytes);
        warn "timer";
        
        
    };
    $conns{$hd}{timer} = $timer;
    $conns{$hd}{db_hd} = $db_io;
    $conns{$hd}{hs}    = $hs;
    $conns{$hd}{hd}    = $hd;
}
=encoding utf8

=head1 AUTHOR

A clever guy

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

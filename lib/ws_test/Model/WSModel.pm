package ws_test::Model::WSModel;

use strict;
use warnings;
use parent 'Catalyst::Model::DBI';

__PACKAGE__->config(
  dsn           => 'dbi:Pg:dbname=ws_test;host=127.0.0.1',
  user          => 'ws_test',
  password      => '123qwe',
  options       => {},
);

=head1 NAME

ws_test::Model::WSModel - DBI Model Class

=head1 SYNOPSIS

See L<ws_test>

=head1 DESCRIPTION

DBI Model Class.

=head1 AUTHOR

A clever guy

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
